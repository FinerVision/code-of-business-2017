<?php

return [
    (object)[
        'id' => 'governance',
        'title' => 'policies:content:governance:title',
        'description' => 'policies:content:governance:description',
        'items' => [
            (object)[
                'title' => 'policies:content:governance:item1:title',
                'description' => 'policies:content:governance:item1:description',
            ],
            (object)[
                'title' => 'policies:content:governance:item2:title',
                'description' => 'policies:content:governance:item2:description',
            ],
            (object)[
                'title' => 'policies:content:governance:item3:title',
                'description' => 'policies:content:governance:item3:description',
            ],
        ],
    ],
    (object)[
        'id' => 'countering_corruption',
        'title' => 'policies:content:countering_corruption:title',
        'description' => 'policies:content:countering_corruption:description',
        'items' => [
            (object)[
                'title' => 'policies:content:countering_corruption:item1:title',
                'description' => 'policies:content:countering_corruption:item1:description',
            ],
            (object)[
                'title' => 'policies:content:countering_corruption:item2:title',
                'description' => 'policies:content:countering_corruption:item2:description',
            ],
            (object)[
                'title' => 'policies:content:countering_corruption:item3:title',
                'description' => 'policies:content:countering_corruption:item3:description',
            ],
            (object)[
                'title' => 'policies:content:countering_corruption:item4:title',
                'description' => 'policies:content:countering_corruption:item4:description',
            ],
            (object)[
                'title' => 'policies:content:countering_corruption:item5:title',
                'description' => 'policies:content:countering_corruption:item5:description',
            ],
            (object)[
                'title' => 'policies:content:countering_corruption:item6:title',
                'description' => 'policies:content:countering_corruption:item6:description',
            ],
        ],
    ],
    (object)[
        'id' => 'respecting_people',
        'title' => 'policies:content:respecting_people:title',
        'description' => 'policies:content:respecting_people:description',
        'items' => [
            (object)[
                'title' => 'policies:content:respecting_people:item1:title',
                'description' => 'policies:content:respecting_people:item1:description',
            ],
            (object)[
                'title' => 'policies:content:respecting_people:item2:title',
                'description' => 'policies:content:respecting_people:item2:description',
            ],
        ],
    ],
    (object)[
        'id' => 'engaging_externally',
        'title' => 'policies:content:engaging_externally:title',
        'description' => 'policies:content:engaging_externally:description',
        'items' => [
            (object)[
                'title' => 'policies:content:engaging_externally:item1:title',
                'description' => 'policies:content:engaging_externally:item1:description',
            ],
            (object)[
                'title' => 'policies:content:engaging_externally:item2:title',
                'description' => 'policies:content:engaging_externally:item2:description',
            ],
            (object)[
                'title' => 'policies:content:engaging_externally:item3:title',
                'description' => 'policies:content:engaging_externally:item3:description',
            ],
            (object)[
                'title' => 'policies:content:engaging_externally:item4:title',
                'description' => 'policies:content:engaging_externally:item4:description',
            ],
            (object)[
                'title' => 'policies:content:engaging_externally:item5:title',
                'description' => 'policies:content:engaging_externally:item5:description',
            ],
            (object)[
                'title' => 'policies:content:engaging_externally:item6:title',
                'description' => 'policies:content:engaging_externally:item6:description',
            ],
            (object)[
                'title' => 'policies:content:engaging_externally:item7:title',
                'description' => 'policies:content:engaging_externally:item7:description',
            ],
            (object)[
                'title' => 'policies:content:engaging_externally:item8:title',
                'description' => 'policies:content:engaging_externally:item8:description',
            ],
        ],
    ],
    (object)[
        'id' => 'safeguarding_information',
        'title' => 'policies:content:safeguarding_information:title',
        'description' => 'policies:content:safeguarding_information:description',
        'items' => [
            (object)[
                'title' => 'policies:content:safeguarding_information:item1:title',
                'description' => 'policies:content:safeguarding_information:item1:description',
            ],
            (object)[
                'title' => 'policies:content:safeguarding_information:item2:title',
                'description' => 'policies:content:safeguarding_information:item2:description',
            ],
            (object)[
                'title' => 'policies:content:safeguarding_information:item3:title',
                'description' => 'policies:content:safeguarding_information:item3:description',
            ],
            (object)[
                'title' => 'policies:content:safeguarding_information:item4:title',
                'description' => 'policies:content:safeguarding_information:item4:description',
            ],
            (object)[
                'title' => 'policies:content:safeguarding_information:item5:title',
                'description' => 'policies:content:safeguarding_information:item5:description',
            ],
        ],
    ],
];
