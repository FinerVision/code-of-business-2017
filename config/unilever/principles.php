<?php

return [
    [
        'type' => 'text',
        'title' => 'principles:grid:box1:title',
        'body' => 'principles:grid:box1:body',
        'bg' => 'turquoise',
    ],
    [
        'type' => 'text',
        'title' => 'principles:grid:box2:title',
        'body' => 'principles:grid:box2:body',
        'bg' => 'blue',
    ],
    [
        'type' => 'img',
        'img' => 'principles:grid:box3:image',
    ],
    [
        'type' => 'img',
        'img' => 'principles:grid:box4:image',
        'col' => 'two'
    ],
    [
        'type' => 'text',
        'title' => 'principles:grid:box5:title',
        'body' => 'principles:grid:box5:body',
        'bg' => 'smoke',
        'color' => 'turquoise',
    ],
    [
        'type' => 'text',
        'title' => 'principles:grid:box6:title',
        'body' => 'principles:grid:box6:body',
        'bg' => 'gold'
    ],
    [
        'type' => 'text',
        'title' => 'principles:grid:box7:title',
        'body' => 'principles:grid:box7:body',
        'bg' => 'smoke',
        'color' => 'turquoise',
    ],
    [
        'type' => 'img',
        'img' => 'principles:grid:box8:image',
        'row' => 'two'
    ],
    [
        'type' => 'text',
        'title' => 'principles:grid:box9:title',
        'body' => 'principles:grid:box9:body',
        'bg' => 'blue',
        'col' => 'two',
        'row' => 'two',
    ],
    [
        'type' => 'text',
        'title' => 'principles:grid:box10:title',
        'body' => 'principles:grid:box10:body',
        'bg' => 'turquoise'
    ],
    [
        'type' => 'text',
        'title' => 'principles:grid:box11:title',
        'body' => 'principles:grid:box11:body',
        'bg' => 'gold'
    ],
    [
        'type' => 'img',
        'img' => 'principles:grid:box12:image',
    ],
    [
        'type' => 'text',
        'title' => 'principles:grid:box13:title',
        'body' => 'principles:grid:box13:body',
        'bg' => 'smoke',
        'color' => 'turquoise'
    ],
    [
        'type' => 'text',
        'title' => 'principles:grid:box14:title',
        'body' => 'principles:grid:box14:body',
        'bg' => 'turquoise'
    ],
    [
        'type' => 'text',
        'title' => 'principles:grid:box15:title',
        'body' => 'principles:grid:box15:body',
        'bg' => 'blue',
        'col' => 'two'
    ],
    [
        'type' => 'img',
        'img' => 'principles:grid:box16:image',
        'col' => 'two'
    ],
    [
        'type' => 'text',
        'title' => 'principles:grid:box17:title',
        'body' => 'principles:grid:box17:body',
        'bg' => 'gold'
    ],
    [
        'type' => 'text',
        'title' => 'principles:grid:box18:title',
        'body' => 'principles:grid:box18:body',
        'bg' => 'smoke',
        'color' => 'turquoise'
    ],
    [
        'type' => 'text',
        'title' => 'principles:grid:box19:title',
        'body' => 'principles:grid:box19:body',
        'bg' => 'turquoise',
        'col' => 'two',
    ],
];
