<?php

return [
    'home' => 'Home Page',
    'video' => 'Video Page',
    'principles' => 'Principles Page',
    'policies' => 'Policies Page',
    'pledge' => 'Pledges Page',
    'poster' => 'Poster Page',
];
