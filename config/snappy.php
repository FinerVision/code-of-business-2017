<?php

return [
    'pdf' => [
        'enabled' => true,
        'binary' => PHP_OS === 'Linux' ? base_path('vendor/h4cc/wkhtmltopdf-amd64/bin/wkhtmltopdf-amd64') : '/usr/local/bin/wkhtmltopdf',
        'timeout' => false,
        'options' => [],
        'env' => [],
    ],
    'image' => [
        'enabled' => true,
        'binary' => PHP_OS === 'Linux' ? base_path('vendor/h4cc/wkhtmltoimage-amd64/bin/wkhtmltoimage-amd64') : '/usr/local/bin/wkhtmltoimage',
        'timeout' => false,
        'options' => [],
        'env' => [],
    ],
];
