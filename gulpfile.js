const elixir = require('laravel-elixir');
require('laravel-elixir-browserify-official');

elixir.config.css.autoprefix.options.browsers = ['last 15 versions'];

elixir(mix => {
    mix.sass('app.scss');
    mix.sass('poster.scss');
    mix.browserify('app.js');
    mix.browserify('admin.js');

    if (process.env.NODE_ENV === 'production') {
        mix.version([
            'public/css/app.css',
            'public/js/app.js',
            'public/js/admin.js',
        ], 'public');
    }
});
