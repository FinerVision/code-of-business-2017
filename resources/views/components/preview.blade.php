<html>
    <head>
        <style>
            {{ \File::get(public_path('css/poster.css')) }}
        </style>
    </head>

    <body>
        <div>
            <table class="PosterPreview PosterPreview--{{ $color }}">
                <tr>
                    <td>
                        <img src="{{ $image }}" class="PosterPreview__image"/>
                    </td>
                    <td>
                        <img src="{{ $template }}" class="PosterPreview__template"/>
                    </td>
                    <td>
                        <p class="PosterPreview__message">
                            {{ $message }}<br />
                            {{ $name }} &mdash; {{ $country }}
                        </p>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
