@if ($posters->count() > 0)
    <section class="Poster__gallery">
        @if (isset($title))
            <h3>Gallery</h3>
        @endif

        <div class="Poster__gallery-inner">
            <div class="carousel-component">
                <div class="Poster__gallery-items carousel-children">
                    @foreach ($posters as $poster)
                        <img class="Poster__gallery-item" src="{{ asset($poster->poster) }}">
                    @endforeach
                </div>

                <div class="Poster__gallery-arrows">
                    <div class="Poster__gallery-arrow Poster__gallery-arrow-left carousel-prev">&lt;</div>
                    <div class="Poster__gallery-arrow Poster__gallery-arrow-right carousel-next">&gt;</div>
                </div>
            </div>
        </div>

        <div class="Poster__gallery-viewer">
            <div class="Poster__gallery-viewer-close">×</div>
            <img src="" class="Poster__gallery-viewer-img img-responsive"/>
        </div>
    </section>
@endif
