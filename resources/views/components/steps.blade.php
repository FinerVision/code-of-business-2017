<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="Steps">
                <div class="Steps__step Steps__step--home {{ ($active ?? '') === 'watch' ? 'active' : '' }}">
                    <a class="Steps__step-link {{ ($active ?? '') === 'watch' ? 'active' : '' }}" href="{{ ($active ?? '') === 'watch' ? url('video') : 'javascript:void(0)' }}">
                        @if(isset($ticked) && is_array($ticked) && in_array('watch', $ticked))
                            <div class="Steps__tick-icon"></div>
                        @endif
                        <div class="Steps__step-content">
                            <img src="{{ asset('img/icons/watch.png') }}"/>
                            <p class="Steps__step-title font-besom text-turquoise">Watch</p>
                            <p class="Steps__step-text">our short video</p>
                        </div>
                    </a>
                    <img src="{{ asset('img/arrow.png') }}" class="Steps__step-arrow">
                    <div>&nbsp;</div>
                </div>

                <div class="Steps__step Steps__step--video {{ ($active ?? '') === 'read' ? 'active' : '' }}">
                    <a class="Steps__step-link {{ ($active ?? '') === 'read' ? 'active' : '' }}" href="{{ ($active ?? '') === 'read' ? ($activePhrase === '1' ? url('principles') : url('policies')) : 'javascript:void(0)' }}">
                        @if(isset($ticked) && is_array($ticked) && in_array('read', $ticked))
                            <div class="Steps__tick-icon"></div>
                        @endif
                        <div class="Steps__step-content">
                            <img src="{{ asset('img/icons/read.png') }}"/>
                            <p class="Steps__step-title font-besom text-turquoise">Read</p>
                            <p class="Steps__step-text">our Code of Business Principles and Code Policies</p>
                        </div>
                    </a>
                    <img src="{{ asset('img/arrow.png') }}" class="Steps__step-arrow">
                    <div>&nbsp;</div>
                </div>

                <div class="Steps__step Steps__step--principles {{ ($active ?? '') === 'sign' ? 'active' : '' }}">
                    <a class="Steps__step-link {{ ($active ?? '') === 'sign' ? 'active' : '' }}" href="{{ ($active ?? '') === 'sign' ? url('pledge') : 'javascript:void(0)' }}">
                        @if(isset($ticked) && is_array($ticked) && in_array('sign', $ticked))
                            <div class="Steps__tick-icon"></div>
                        @endif
                        <div class="Steps__step-content">
                            <img src="{{ asset('img/icons/sign.png') }}"/>
                            <p class="Steps__step-title font-besom text-turquoise">Sign</p>
                            <p class="Steps__step-text">your Code Declaration</p>
                        </div>
                    </a>
                    <img src="{{ asset('img/arrow.png') }}" class="Steps__step-arrow">
                    <div>&nbsp;</div>
                </div>

                <div class="Steps__step Steps__step--policies {{ ($active ?? '') === 'share' ? 'active' : '' }}">
                    <a class="Steps__step-link {{ ($active ?? '') === 'share' ? 'active' : '' }}" href="{{ ($active ?? '') === 'share' ? url('poster') : 'javascript:void(0)' }}">
                        @if(isset($ticked) && is_array($ticked) && in_array('share', $ticked))
                            <div class="Steps__tick-icon"></div>
                        @endif
                        <div class="Steps__step-content">
                            <img src="{{ asset('img/icons/share.png') }}"/>
                            <p class="Steps__step-title font-besom text-turquoise">Share</p>
                            <p class="Steps__step-text">your Integrity Pledge poster</p>
                        </div>
                    </a>
                </div>

            </div>
        </div>
    </div>
</div>
