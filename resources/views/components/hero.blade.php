<section class="Hero {{ isActive('/') ? 'Hero--longer' : '' }}">
    @if (isActive('/'))
        <div class="Hero__text">
                @include('components.scroll-indicator')
        </div>
    @endif

    @if (isActive('video'))
        <div
                class="video-component"
                data-src="{{ asset('vid/hero2.mp4') }}"
                data-poster="{{ asset('img/video-placeholder.png') }}"
        >
        </div>
    @endif
</section>
