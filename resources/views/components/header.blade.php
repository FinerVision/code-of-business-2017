<header class="Header">
    <div class="Header__container">
        <a href="{{ url('/') }}">
            <img src="{{ asset('img/logo.svg') }}" class="logo">
        </a>

        <nav>
            <div class="mobile-menu">
                <div></div>
                <div></div>
                <div></div>
            </div>

            <a href="{{ url('/') }}" class="{{ getActive('/') }}">Home</a>
            <a href="{{ url('/video') }}" class="{{ getActive('video') }}">Video</a>
            <a href="{{ url('/principles') }}" class="{{ getActive('principles') }}">Principles</a>
            <a href="{{ url('/policies') }}" class="{{ getActive('policies') }}">Policies</a>
            <a href="{{ url('/pledge') }}" class="{{ getActive('pledge') }}">Pledge</a>
            <a href="{{ url('/poster') }}" class="{{ getActive('poster') }}">Your Poster</a>
        </nav>
    </div>
</header>
