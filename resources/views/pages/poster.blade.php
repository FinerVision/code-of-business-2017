@extends('layouts.default')

@section('content')
    <div class="Page Poster">
        <main class="container">

            <div class="container-content">
                <div class="intro-boxes">
                    <div class="Poster__title intro-box intro-box--title">
                        {{ content('poster:intro:title') }}
                    </div>

                    <div class="intro-box">
                        {!! contentPTags('poster:intro:body') !!}
                    </div>
                </div>

                <br />
                <br />
                <br />
                @include('components.scroll-indicator')

                <div
                    class="poster-editor-component"
                    data-values="{{ json_encode(contentGroup('poster:editor')) }}"
                ></div>

                @include('components.gallery-viewer', [
                    'posters' => $posters,
                    'title' => 'Gallery'
                ])

                <section class="next-step">
                    <a href="{{ url('/') }}" class="Button Button--next-step">
                        <img src="{{ asset('img/return-button.png') }}">
                    </a>
                </section>
            </div>
        </main>
    </div>
@endsection
