@extends('layouts.default')

@php
    $active = 'governance';
@endphp

@section('content')
    <div class="Page Policies">
        <nav class="MenuNav">
            <div class="MenuNav__inner">
                @foreach($policies as $policy)
                    <div class="MenuNav__item MenuNav__item--{{ $policy->id }} {{ $policy->id === $active ? 'active' : '' }}"
                         data-policy="{{ $policy->id }}">
                        <span>{!! preg_replace('/\s/', '<br/>', content($policy->title)) !!}</span>
                    </div>
                @endforeach
            </div>
        </nav>

        <main>

            <div class="Policies__container">

                <div class="container-content">
                    <p class="Policies__intro download-pdf font-besom text-turquoise">
                        {{ content('policies:intro') }}
                    </p>

                    <p class="Policies__lead download-pdf">
                        {{ content('policies:download:text') }}
                        <a
                            href="{{ content('policies:download:link') }}"
                            target="_blank" class="link"
                        >
                            {{ content('policies:download:link_text') }}
                        </a>.
                    </p>
                </div>

                @include('components.scroll-indicator')

                @foreach($policies as $policy)
                    <section class="boxes policy policy--{{ $policy->id }} {{ $policy->id === $active ? 'policy--shown' : '' }}"
                             data-policy="{{ $policy->id }}">
                        @foreach($policy->items as $item)
                            <div class="box">
                                <h5 class="font-besom">{{ strtoupper(content($item->title)) }}</h5>
                                <hr/>
                                {{ content($item->description) }}
                            </div>
                        @endforeach
                    </section>
                @endforeach

                <br/>
                <br/>

                @include('components.steps', [
                    'active' => 'sign',
                    'ticked' => ['watch', 'read']
                ])

                <section class="next-step">
                    <a href="{{ url('pledge') }}" class="Button Button--next-step">
                        <img src="{{ asset('img/sign-button.png') }}">
                    </a>
                </section>
            </div>
        </main>
    </div>

    <script>
        window.activePolicy = '{{ $active }}';
    </script>
@endsection
