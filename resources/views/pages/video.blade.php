@extends('layouts.default')

@section('content')
    <div class="Page Video">
        @include('components.hero')

        <main>
            <section class="bg-turquoise">
                @include('components.scroll-indicator')

                <div class="container-content">
                    {!! contentPTags('video:intro') !!}
                    <h5 class="font-besom text-center">{{ content('video:title') }}</h5>
                </div>
            </section>

            <br/>
            <br/>

            @include('components.steps', [
                'active' => 'read',
                'activePhrase' => '1',
                'ticked' => ['watch']
            ])

            <section class="next-step">
                <a href="{{ url('principles') }}" class="Button Button--next-step">
                    <img src="{{ asset('img/read-principles-button.png') }}">
                </a>
            </section>
        </main>
    </div>
@endsection
