@extends('layouts.default')

@section('content')
    <div class="Page Pledge">
        <main>
            <div class="container">

                <div class="container-content">
                    <div class="intro-boxes intro-boxes--reverse">
                        <div class="Pledge__tracker-intro-box intro-box">
                            <h3 class="font-besom">{{ content('pledge:ticker:title') }}</h3>

                            <hr/>

                            <div class="pledge-total">
                                @foreach (str_split($total) as $number)
                                    <div>{{ $number }}</div>
                                @endforeach
                            </div>
                            <div class="Pledge__tracker-statement">
                                {{ content('pledge:ticker:statement') }}
                            </div>
                        </div>

                        <div class="intro-box intro-box--small">
                            <p>
                                {{ content('pledge:signing:statement') }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            @include('components.scroll-indicator')

            <p class="info">{{ content('pledge:button:caption') }}</p>

            <div class="title-hr">
                <a target="_blank" href="{{ content('pledge:button:link') }}">
                    <h4>{{ content('pledge:button:text') }}</h4>
                </a>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <p class="info info--gallery">
                            {{ content('pledge:gallery:intro') }}
                        </p>

                        @include('components.gallery-viewer', ['posters' => $posters])
                    </div>
                </div>
            </div>


            @include('components.steps', [
                'active' => 'share',
                'ticked' => ['watch','read','sign'],
            ])

            <section class="next-step">
                <a href="{{ url('poster') }}" class="Button Button--next-step">
                    <img src="{{ asset('img/share-button.png') }}">
                </a>
            </section>
        </main>
    </div>
@endsection
