@extends('layouts.default')

@section('content')
    <div class="Page Holding">
        <img src="{{ asset('img/holding_logo.svg') }}" class="logo">

        <main class="text-center">
            <h1 class="font-besom">INTEGRITY PLEDGE WEEK</h1>

            <h2>
                Launching:<br/>
                Monday 20th November, 2017
            </h2>
        </main>
    </div>
@endsection
