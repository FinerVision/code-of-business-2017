@extends('layouts.default')

@section('content')
    <div class="Page Principles">
        <main class="container">

            <div class="intro-text text-center">
                <p class="font-besom">
                    {{ content('principles:intro') }}

                </p>
                <h2 class="font-besom">{{ content('principles:slogan') }}</h2>
            </div>

            @include('components.scroll-indicator')

            <div
                class="principles-grid-component"
                data-elements="{{ json_encode($principles) }}"
                data-values="{{ json_encode(contentGroup('principles:grid')) }}"
            ></div>

            <br/>
            <br/>

            @include('components.steps', [
                'active' => 'read',
                'activePhrase' => '2',
                'ticked' => ['watch'],
            ])

            <section class="next-step">
                <a href="{{ url('policies') }}" class="Button Button--next-step">
                    <img src="{{ asset('img/read-policies-button.png') }}">
                </a>
            </section>
        </main>
    </div>
@endsection
