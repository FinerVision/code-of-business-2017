@extends('layouts.default')

@section('content')
    <div class="Page Home">
        @include('components.hero')

        <main>
            <br />

            <h1 class="font-besom text-center text-turquoise">{{ content('home:welcome') }}</h1>
            <p class="font-din text-center">
                <strong>{{ content('home:intro') }}</strong>
            </p>

            <h3 class="Home__steps-title text-uppercase font-besom underline underline-turquoise d-table centre">Steps</h3>

            @include('components.steps', [
                'active' => content('home:intro'),
                'ticked' => [],
            ])

            <section class="next-step">
                <a href="{{ url('video') }}" class="Button Button--next-step">
                    <img src="{{ asset('img/watch-button.png') }}">
                </a>
            </section>
        </main>
    </div>
@endsection
