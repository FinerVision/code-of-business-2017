<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="author" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

        <title>Code of Business</title>

        @if (env('APP_ENV') === 'production')
            <link rel="stylesheet" href="{{ asset(elixir('css/app.css', null)) }}">
        @else
            <link rel="stylesheet" href="{{ asset('css/app.css') }}?v={{ time() }}">
        @endif

        <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
        <link rel="apple-touch-icon" href="{{ asset('img/apple-touch-icon.png') }}">
    </head>

    <body class="page-{{ request()->path() }}">
        <div id="wrapper">
            @if (request()->path() !== 'holding')
                @include('components.header')
            @endif

            <div id="container">
                @yield('content')

                @include('components.footer')
            </div>
        </div>

        @if (env('APP_ENV') === 'production')
            <script src="{{ asset(elixir('js/app.js', null)) }}"></script>
        @else
            <script src="{{ asset('js/app.js') }}?v={{ time() }}"></script>
        @endif

        @if (request()->getHost() === 'integritypledge.unilever.com')
            <script async src="https://www.googletagmanager.com/gtag/js?id=UA-72689622-7"></script>
            <script>
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());

                gtag('config', 'UA-72689622-7');
            </script>
        @endif
    </body>
</html>
