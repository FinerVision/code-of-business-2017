@extends('auth.admin')


@section('page-title')
    View Posters
@endsection

@section('content')
<div class="container">
    <br />
    <div class="row">
        @foreach($postersPaginate as $poster)
            <div class="col-lg-3">
                <img class="img-fluid" src="{{ $poster->poster }}" />
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td colspan="2"><br />{{ $poster->created_at }}</td>
                    </tr>
                    <tr>
                        <th>name</th>
                        <td>{{ $poster->name }}</td>
                    </tr>
                    <tr>
                        <th>message</th>
                        <td>{{ $poster->message }}</td>
                    </tr>
                    <tr>
                        <th>country</th>
                        <td>{{ $poster->country }}</td>
                    </tr>
                    <tr>
                        <th>poster</th>
                        <td>
                            <a download href="{{ $poster->poster }}" target="_blank"><i class="fa fa-external-link"></i></a>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        @endforeach
    </div>
    <div class="navigation">
        {{ $postersPaginate->render() }}
    </div>
</div>
@endsection
