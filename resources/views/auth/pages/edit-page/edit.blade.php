@extends('auth.admin')


@section('page-title')
    Edit Pages {{ config("page.sitePages.{$pageKey}") }}
@endsection

@section('content')
    <div class="container">


        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <br />
        <form method="POST" action="{{ route('admin.edit-page.edit', $pageKey) }}">
            <div class="">
                <h2>Edit Page - {{ $pageName }}</h2>
                <a class="btn btn-primary" href="{{ route('admin.edit-page') }}">Back</a>

                <table class="table">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Content</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($contents as $content)
                        <tr>
                            <td>
                                {{ $content->title }}
                                @if ($content->type === 'image')
                                    <img style="display: block; max-width: 200px;" src="{{ $content->value }}" />
                                @endif
                            </td>
                            <td>
                                @switch($content->type)
                                    @case('textmulti')
                                        <textarea class="form-control" rows="5" name="{{ $content->key }}">{{ old($content->key,$content->value) }}</textarea>
                                        @break
                                    @case('image')
                                        <div data-component="ImageUpload" data-props="{{ json_encode([
                                            'name' => $content->key,
                                            'defaultValue' => $content->value,
                                        ]) }}"></div>
                                        @break
                                    @case('text')
                                    @case('link')
                                    @default
                                        <input type="text" class="form-control" name="{{ $content->key }}" value="{{ old($content->key) ?? $content->value }}" placeholder />
                                @endswitch
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <br />
                <br />
                <button type="submit" class="btn btn-primary float-right">Submit Changes</button>
                <br />
                <br />
                <br />
                <br />
            </div>
        </form>
    </div>
@endsection
