@extends('auth.admin')


@section('page-title')
    Edit Pages
@endsection

@section('content')
    <div class="container">

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif


        <br />
        <form method="POST" action="{{ route(currentRoute()) }}">
            <div class="input-group mb-2">
                <span class="input-group-addon" id="basic-addon1">Update Pledge Ticker</span>
                <input type="text" class="form-control" placeholder="Pledge Ticker" name="tickerNumber" value="{{ Content::byKeySingle('pledge:ticker:number') }}">
            </div>
            <button type="submit" class="btn btn-primary float-right">Update Pledge Ticker</button>
        </form>
        <br />
        <table class="table">
            <thead>
            <tr>
                <th>Name</th>
                <th>Latest Editing</th>
                <th>Edit</th>
            </tr>
            </thead>
            <tbody>
            @foreach(config('admin.sitePages') as $keyPrefix => $pageTitle)
                <tr>
                    <td>{{ $pageTitle }}</td>
                    <td>{{ Content::latestEditingByPrefix($keyPrefix) }}</td>
                    <td>
                        <a class="btn btn-primary" href="{{ route('admin.edit-page.edit', $keyPrefix) }}">Edit</a>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
@endsection
