<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('page-title', 'Page') - Code of Business</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha256-QUyqZrt5vIjBumoqQV0jM8CgGqscFfdGhN+nVCqX0vc=" crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />

</head>
<body>
<nav class="navbar navbar-dark navbar-expand bg-dark">
    <div class="container">
        <div class="navbar-brand">
            Poster - Code Of Business
        </div>

        <div class="collapse navbar-collapse">
            <div class="navbar-nav">
                @foreach(config('admin.nav.items') as $navRouteName => $navTitle)
                    <a class="{{ classNames('nav-item', 'nav-link', [ 'active' => isCurrentRoute($navRouteName) ]) }}" href="{{ route($navRouteName) }}">{{ $navTitle }}</a>
                @endforeach
            </div>
        </div>

        <a href="{{ route('logout') }}"
           style="color: white;"
           onclick="event.preventDefault();document.getElementById('logout-form').submit();"
        >
            Logout
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            {{ csrf_field() }}
        </form>
    </div>


</nav>

@yield('content')


<script type="text/javascript" src="https://asset-store.finervision.com/js/AssetStore.js"></script>
<script type="text/javascript" src="https://unpkg.com/jquery@3.2.1/dist/jquery.js"></script>
@if (env('APP_ENV') === 'production')
    <script src="{{ asset(elixir('js/admin.js', null)) }}"></script>
@else
    <script src="{{ asset('js/admin.js') }}?v={{ time() }}"></script>
@endif
</body>
</html>







