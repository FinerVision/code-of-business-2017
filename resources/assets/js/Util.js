export default {
    /**
     * @return {boolean}
     */
    IE(v) {
        return new RegExp('msie' + (!isNaN(v)?('\\s'+v):''), 'i').test(navigator.userAgent);
    }
};
