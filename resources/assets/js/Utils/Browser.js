import {detect} from "detect-browser";

window.addEventListener('load', () => {
    const browser = detect();
    const html = document.documentElement;
    let newClasses = html.getAttribute('class') || '';
    const browserVersionMajor = (browser.version || '').split('.')[0] || 'unknown';
    newClasses += ` ${browser.name}-browser`;
    newClasses += ` ${browserVersionMajor}-browser-version`;
    newClasses = newClasses.trim();
    html.setAttribute('class', newClasses);
});
