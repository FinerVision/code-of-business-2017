import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";

class Video extends Component {
    constructor(props) {
        super(props);

        this.state = {
            playing: false
        };
    }

    componentDidMount() {
        this.refs.video.addEventListener('play', () => this.setState({playing: true}));
        this.refs.video.addEventListener('ended', () => this.setState({playing: false}));
    }

    play() {
        this.refs.video.play();
        this.setState({playing: true}); // force the state to be changed when playing (this fix is for IE browser, poster still covering for the 2nd play)
    }

    render() {
        return (
            <div className="Video">
                {this.state.playing ? null : (
                    <div className="Video__img" style={{backgroundImage: `url(${this.props.poster})`}}>
                        <img src="img/play_button.png" className="Video__play-button" onClick={() => this.play()}/>
                    </div>
                )}

                <video preload="auto" controls={true} ref="video">
                    <source src={this.props.src} type="video/mp4"/>
                </video>
            </div>
        );
    }
}

Video.propTypes = {
    src: PropTypes.string.isRequired,
    poster: PropTypes.string.isRequired
};

const nodes = document.querySelectorAll('.video-component');

for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const props = {
        src: node.getAttribute('data-src'),
        poster: node.getAttribute('data-poster')
    };

    render(<Video {...props}/>, node);
}
