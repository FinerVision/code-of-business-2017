import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import Masonry from "react-masonry-component";

const Util = {
    nl2br(str) {
        return String(str).split("\n").join('<br/>');
    }
};

class PrinciplesGrid extends Component {
    constructor(props) {
        super(props);

        this.options = {
            percentPosition: true
        };
    }

    getValue(key) {
        return this.props.values[key];
    }

    getChildContent(element) {
        if (element.type === 'text') {
            return (
                <div className="grid-item-text">
                    <h5 className="grid-item-title">{this.getValue(element.title)}</h5>
                    <p className="grid-item-body" dangerouslySetInnerHTML={{__html: Util.nl2br(this.getValue(element.body))}}/>
                </div>
            );
        }

        if (element.type === 'img') {
            return <div style={{backgroundImage: `url(${this.getValue(element.img)})`}} className="grid-item-img"/>;
        }
    }

    renderChildElements() {
        return this.props.elements.map((element, index) => {
            return (
                <div
                    key={index}
                    className={`
                        grid-item
                        grid-item-bg-${element.bg || 'default'}
                        grid-item-color-${element.color || 'default'}
                        grid-item--${element.type}
                        grid-item--row-${element.row || 'default'}
                        grid-item--col-${element.col || 'default'}
                    `}
                >
                    <div className="grid-item-content">
                        <div className="grid-item-content-inner">
                            {this.getChildContent(element)}
                        </div>
                    </div>
                </div>
            );
        });
    }

    render() {
        console.log(this.props.values);
        return (
            <div className="PrinciplesGrid">
                <Masonry
                    className="grid"
                    elementType="div"
                    options={this.options}
                >
                    {this.renderChildElements()}
                </Masonry>
            </div>
        );
    }
}

PrinciplesGrid.propTypes = {
    elements: PropTypes.array.isRequired,
    values: PropTypes.objectOf(PropTypes.string).isRequired,
};

const nodes = document.querySelectorAll('.principles-grid-component');

for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const props = {
        elements: JSON.parse(node.getAttribute('data-elements') || '[]'),
        values: JSON.parse(node.getAttribute('data-values') || '{}'),
    };

    render(<PrinciplesGrid {...props}/>, node);
}
