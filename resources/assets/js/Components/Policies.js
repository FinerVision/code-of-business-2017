if (window.activePolicy) {
    let active = window.activePolicy;

    function updateBoxes() {
        const policies = document.querySelectorAll('.policy');

        for (let i = 0; i < policies.length; i++) {
            const id = policies[i].getAttribute('data-policy');

            if (id === active) {
                policies[i].classList.add('policy--shown');
            } else {
                policies[i].classList.remove('policy--shown');
            }
        }
    }

    function setActive(policies, id) {
        active = id;

        for (let i = 0; i < policies.length; i++) {
            if (policies[i].getAttribute('data-policy') === active) {
                policies[i].classList.add('active');
            } else {
                policies[i].classList.remove('active');
            }
        }

        updateBoxes();
    }

    updateBoxes();

    const policies = document.querySelectorAll('.MenuNav__item');

    for (let i = 0; i < policies.length; i++) {
        const id = policies[i].getAttribute('data-policy');
        policies[i].addEventListener('click', setActive.bind(null, policies, id));
    }
}
