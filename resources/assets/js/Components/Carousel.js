class Carousel {
    constructor(container) {
        this.container = container;
        this.carouselChildrenContainer = this.container.querySelector('.carousel-children');
        this.nextButton = this.container.querySelector('.carousel-next');
        this.prevButton = this.container.querySelector('.carousel-prev');

        this.maxChildren = 15;
        this.slidesToShow = 3;

        this.state = {
            activeIndex: 0,
            children: []
        };

        this.setChildren();

        this.next = this.next.bind(this);
        this.prev = this.prev.bind(this);

        this.nextButton.addEventListener('click', this.next);
        this.prevButton.addEventListener('click', this.prev);

        this.update();
    }

    setChildren() {
        const carouselChildren = this.carouselChildrenContainer.children;
        const children = [];

        for (let i = 0; i < carouselChildren.length; i++) {
            const child = carouselChildren[i];
            child.index = i;
            child.active = i === this.state.activeIndex;
            children.push(child);
        }

        this.state.children = children;
    }

    next() {
        const {activeIndex, children} = this.state;
        this.state.activeIndex = activeIndex === children.length - 1 ? 0 : activeIndex + 1;
        this.update();
    }

    prev() {
        const {activeIndex, children} = this.state;
        this.state.activeIndex = activeIndex === 0 ? children.length - 1 : activeIndex - 1;
        this.update();
    }

    update() {
        let {activeIndex, children} = this.state;

        children = children.map(child => {
            child.active = child.index === activeIndex;
            return child;
        });

        children.map(child => {
            if (child.active) {
                child.classList.add('carousel-active');
            } else {
                child.classList.remove('carousel-active');
            }
        });

        const total = Math.max(0, children.length - this.slidesToShow);
        let translateX = (100 / this.slidesToShow) * activeIndex;

        this.nextButton.style.visibility = activeIndex >= total ? 'hidden' : 'visible';
        this.prevButton.style.visibility = activeIndex === 0 ? 'hidden' : 'visible';
        this.carouselChildrenContainer.style.transform = `translateX(-${translateX}%)`;
    }
}

const nodes = document.querySelectorAll('.carousel-component');

for (let i = 0; i < nodes.length; i++) {
    new Carousel(nodes[i]);
}
