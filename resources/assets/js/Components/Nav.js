const body = document.querySelector('body');
const container = document.querySelector('#container');
const menu = document.querySelector('.mobile-menu');

if (menu) {
    menu.addEventListener('click', () => {
        container.scrollTop = 0;

        menu.classList.toggle('open');

        if (menu.classList.contains('open')) {
            body.classList.add('mobile-menu-open');
        } else {
            body.classList.remove('mobile-menu-open');
        }
    });
}
