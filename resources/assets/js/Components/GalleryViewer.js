const viewer = document.querySelector('.Poster__gallery-viewer');

if (viewer) {
    const close = document.querySelector('.Poster__gallery-viewer-close');
    const items = document.querySelectorAll('.Poster__gallery-item');

    function removeOpenClassName(event) {
        if (event.target.nodeName === 'IMG') {
            return;
        }

        viewer.classList.remove('Poster__gallery-viewer--open');
        viewer.removeEventListener('click', removeOpenClassName);
    }

    function toggleViewer(src) {
        viewer.classList.toggle('Poster__gallery-viewer--open');
        viewer.querySelector('img').setAttribute('src', src);
        viewer.addEventListener('click', removeOpenClassName);
    }

    for (let i = 0; i < items.length; i++) {
        const item = items[i];
        const src = item.getAttribute('src');
        item.addEventListener('click', () => toggleViewer(src));
    }

    close.addEventListener('click', removeOpenClassName);
}
