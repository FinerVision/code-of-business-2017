import React, {Component} from "react";
import {render} from "react-dom";
import PropTypes from "prop-types";
import request from "superagent";
import Util from "../../Util";
import Template from "./Template";
import {detect} from "detect-browser";

const MESSAGE_LIMIT = 44;
const NAME_LIMIT = 100;
const COUNTRY_LIMIT = 100;
const DEFAULT_MESSAGE = 'AT UNILEVER, WE ARE CHANGING THE WAY BUSINESS IS DONE';
const DEFAULT_STATE = {
    submitted: false,
    uploaded: false,
    uploadedImage: null,
    uploadProgress: 0,
    galleryOpen: false,
    submitting: false,
    messageCount: 0,
    message: '',
    name: '',
    country: '',
    color: 'white',
    poster: null,
    errors: {},
    images: [
        {
            src: 'img/gallery/earth.png',
            selected: true,
            chosen: true
        },
        {
            src: 'img/gallery/hands.png',
            selected: false,
            chosen: false
        },
        {
            src: 'img/gallery/lighthouse.png',
            selected: false,
            chosen: false
        },
        {
            src: 'img/gallery/plant.png',
            selected: false,
            chosen: false
        },
        {
            src: 'img/gallery/umbrellas.jpg',
            selected: false,
            chosen: false
        },
        {
            src: 'img/gallery/Holi-fest.jpg',
            selected: false,
            chosen: false
        },
        {
            src: 'img/gallery/md-3.jpg',
            selected: false,
            chosen: false
        },
        {
            src: 'img/gallery/sunshine-2.jpg',
            selected: false,
            chosen: false
        },
        {
            src: 'img/gallery/2b-2.jpg',
            selected: false,
            chosen: false
        }
    ]
};

class PosterEditor extends Component {
    constructor(props) {
        super(props);

        this.timeout = null;

        this.colors = {
            orange: '#e47100',
            blue: '#004876',
            white: '#fff',
        };

        this.state = DEFAULT_STATE;
    }

    componentDidMount() {
        location.hash = '';
        this.preloadImages();
    }

    componentWillUnmount() {
        this.clearTimeout();
    }

    getValue(key) {
        return this.props.values[key];
    }

    clearTimeout() {
        if (this.timeout !== null) {
            clearTimeout(this.timeout);
            this.timeout = null;
        }
    }

    preloadImages() {
        for (let i = 0; i < this.state.images.length; i++) {
            const img = new Image();
            img.src = this.state.images[i].src;
        }
    }

    toggleGallery(galleryOpen = null) {
        galleryOpen = galleryOpen === null ? !this.state.galleryOpen : galleryOpen;
        this.setState({galleryOpen, uploadedImage: galleryOpen ? null : this.state.uploadedImage});
    }

    isNext(index) {
        const selectedIndex = this.getSelectedImageIndex();

        if (selectedIndex === this.state.images.length - 1 && index === 0) {
            return true;
        }

        return this.state.images[index - 1] && this.state.images[index - 1].selected;
    }

    isPrev(index) {
        const selectedIndex = this.getSelectedImageIndex();

        if (selectedIndex === 0 && index === this.state.images.length - 1) {
            return true;
        }

        return this.state.images[index + 1] && this.state.images[index + 1].selected;
    }

    getSelectedImageIndex() {
        for (let i = 0; i < this.state.images.length; i++) {
            if (this.state.images[i].selected) {
                return i;
            }
        }
    }

    getChosenImageIndex() {
        for (let i = 0; i < this.state.images.length; i++) {
            if (this.state.images[i].chosen) {
                return i;
            }
        }
    }

    next() {
        const {images} = this.state;
        const selectedIndex = this.getSelectedImageIndex();
        const nextSelectedIndex = selectedIndex === images.length - 1 ? 0 : selectedIndex + 1;
        const updatedImages = images.map((image, index) => {
            image.selected = index === nextSelectedIndex;
            return image;
        });
        this.setState({images: updatedImages});
    }

    prev() {
        const {images} = this.state;
        const selectedIndex = this.getSelectedImageIndex();
        const nextSelectedIndex = selectedIndex === 0 ? images.length - 1 : selectedIndex - 1;
        const updatedImages = images.map((image, index) => {
            image.selected = index === nextSelectedIndex;
            return image;
        });
        this.setState({images: updatedImages});
    }

    handleMessageChange(event) {
        const message = event.target.value;
        const messageCount = message.length;

        if (message.trim().length > MESSAGE_LIMIT) {
            // TODO: Show error: message too long.
            console.error(`Message too long. Needs to be ${MESSAGE_LIMIT} characters or less`);
            return;
        }

        this.setState({message, messageCount});
    }

    handleNameChange(event) {
        const name = event.target.value;

        if (name.trim().length > NAME_LIMIT) {
            // TODO: Show error: name too long.
            console.error(`Name too long. Needs to be ${NAME_LIMIT} characters or less`);
            return;
        }

        this.setState({name});
    }

    handleCountryChange(event) {
        const country = event.target.value;

        if (country.trim().length > COUNTRY_LIMIT) {
            // TODO: Show error: country too long.
            console.error(`Country too long. Needs to be ${COUNTRY_LIMIT} characters or less`);
            return;
        }

        this.setState({country});
    }

    selectColor(color) {
        this.setState({color});
    }

    uploadFile(file) {
        if (!file) {
            console.log('Error');
            return;
        }

        const uploadedImage = URL.createObjectURL(file);
        const data = new FormData();
        data.append('file', file);

        this.setState({uploadedImage, uploaded: false}, () => {
            request
                .post('upload')
                .send(data)
                .accept('application/json')
                .on('progress', event => this.setState({uploadProgress: event.percent}))
                .end((err, res) => {
                    this.setState({uploaded: true, uploadedImage: res.body.url}, () => {
                        this.timeout = setTimeout(() => {
                            this.clearTimeout();
                            this.setState({uploadProgress: 0});
                        }, 3000);
                    });
                });
        });
    }

    getProgress() {
        if (this.state.uploadProgress === 0) {
            return <p style={{marginBottom: '2em'}}>{this.getValue('poster:editor:upload_title')}</p>;
        }

        if (this.state.uploaded) {
            return <p>{this.getValue('poster:editor:uploaded_text')}</p>;
        }

        return (
            <div className="progress">
                <div className="progress-bar" style={{width: `${this.state.uploadProgress}%`}}/>
            </div>
        );
    }

    handleSubmit(event) {
        event.preventDefault();

        const {uploadedImage, images, message, name, country, color, uploaded} = this.state;

        // Check if the user has uploaded their own image
        // and that image is uploaded.
        if (uploadedImage && !uploaded) {
            return;
        }

        const image = uploadedImage ? uploadedImage : images[this.getChosenImageIndex()].src;

        const data = new FormData();
        data.append('image', image);
        data.append('message', message);
        data.append('name', name);
        data.append('country', country);
        data.append('color', color);

        this.setState({submitting: true});

        request
            .post('submit')
            .send(data)
            .accept('application/json')
            .end((err, res) => {
                res = res.body;

                if (err) {
                    this.setState({errors: res.errors, submitting: false});
                    return;
                }

                const galleryItems = document.querySelectorAll('.Poster__gallery-items');
                const GALLERY_MAX_CHILDREN = 15;
                const img = document.createElement('img');
                img.className = 'Poster__gallery-item img-responsive';
                img.src = res.poster;

                for (let i = 0; i < galleryItems.length; i++) {
                    const galleryItem = galleryItems[i];

                    if (galleryItem.children.length > GALLERY_MAX_CHILDREN) {
                        galleryItem.removeChild(galleryItem.children[galleryItem.children.length - 1]);
                    }

                    galleryItem.insertBefore(img, galleryItem.children[0]);
                }

                this.setState({errors: {}, submitted: true, submitting: false, poster: res.poster}, () => {
                    requestAnimationFrame(() => requestAnimationFrame(() => location.hash = '#submitted'));
                });
            });
    }

    getErrors() {
        if (Object.keys(this.state.errors).length === 0) {
            return null;
        }

        const errors = [];

        for (let error in this.state.errors) {
            if (this.state.errors.hasOwnProperty(error)) {
                errors.push(this.state.errors[error][0]);
            }
        }

        return (
            <div className="PosterEditor__errors">
                {errors.map((error, index) => <p key={index}>{error}</p>)}
            </div>
        );
    }

    getSubmittingMessage() {
        if (!this.state.submitting) {
            return null;
        }

        return (
            <div className="PosterEditor__submitting">
                <h3>
                    {this.getValue('poster:editor:submitting')}
                </h3>
            </div>
        );
    }

    select(imageIndex) {
        const {images} = this.state;
        const updatedImages = images.map((image, index) => {
            image.chosen = index === imageIndex;
            image.selected = index === imageIndex;
            return image;
        });
        this.setState({images: updatedImages});
        this.toggleGallery(false);
    }

    getMessage() {
        return this.state.message.trim().length === 0 ? DEFAULT_MESSAGE : this.state.message;
    }

    getName() {
        return this.state.name.trim().length === 0 ? '' : `${this.state.name} – `;
    }

    getCountry() {
        return this.state.country.trim();
    }

    reset() {
        location.hash = '';
        this.setState(DEFAULT_STATE);
    }

    render() {
        if (this.state.submitted) {
            return (
                <div id="submitted">
                    <br/><br/><br/>
                    <h3>
                        Thank You<br/>
                        for Sharing
                    </h3>

                    <section className="next-step">
                        <span className="Button Button--next-step" onClick={() => this.reset()}>
                            <img src="img/create_button.png"/>
                        </span>
                        &nbsp;&nbsp;
                        <a href={this.state.poster} download="pledge.png" className="Button Button--next-step">
                            <img src="img/download_button.png"/>
                        </a>
                    </section>
                </div>
            );
        }

        return (
            <form className="PosterEditor" onSubmit={event => this.handleSubmit(event)}>
                {this.getSubmittingMessage()}
                {this.getErrors()}

                <div className="PosterEditor__items">
                    <div className="PosterEditor__item PosterEditor__item--message">
                        <p className="PosterEditor__item-label">{this.getValue('poster:editor:message_title')}</p>
                        <div className="PosterEditor__character-count">max character count: 44</div>
                        <div className="PosterEditor__input-wrap">
                            <input
                                className="PosterEditor__input"
                                onChange={event => this.handleMessageChange(event)}
                                value={this.state.message}
                            />
                        </div>


                        <p className="PosterEditor__item-label">{this.getValue('poster:editor:name_title')}</p>

                        <div className="PosterEditor__input-wrap">
                            <input
                                className="PosterEditor__input"
                                onChange={event => this.handleNameChange(event)}
                                value={this.state.name}
                            />
                        </div>

                        <p className="PosterEditor__item-label">{this.getValue('poster:editor:country_title')}</p>

                        <div className="PosterEditor__input-wrap">
                            <input
                                className="PosterEditor__input"
                                onChange={event => this.handleCountryChange(event)}
                                value={this.state.country}
                            />
                        </div>

                    </div>
                    <div className="PosterEditor__item-group">
                        <div className="PosterEditor__item-group PosterEditor__item-group--column">
                            <div
                                className={`PosterEditor__item PosterEditor__item--upload ${this.state.uploadedImage && !this.state.uploaded ? 'PosterEditor__item--upload-uploading' : ''} outline-box`}
                                style={!this.state.uploadedImage && (this.state.uploadProgress === 0 || this.state.uploaded) ? {} : {backgroundImage: `url(${this.state.uploadedImage})`}}
                                onDrop={event => {
                                    event.preventDefault();
                                    this.uploadFile(event.dataTransfer.files ? event.dataTransfer.files[0] : null);
                                }}
                            >
                                <input
                                    ref="file"
                                    type="file"
                                    max={1}
                                    accept="image/*"
                                    onChange={event => this.uploadFile(event.target.files ? event.target.files[0] : null)}
                                    onClick={() => {
                                        if (Util.IE()) {
                                            this.refs.file.click();
                                        }
                                    }}
                                />
                                <div className="PosterEditor__item--upload-content">
                                    <img src="img/upload_icon.png" className="upload-icon"/>
                                    {this.getProgress()}
                                </div>
                            </div>
                            <br />
                            {['ios', 'crios'].indexOf((detect() || {}).name) !== -1 ? '' : 'OR'}
                            <br />
                            <br />
                            <div className="outline-box" onClick={() => this.toggleGallery()}>
                                <p>{this.getValue('poster:editor:choose_from_gallery_text')}</p>
                            </div>
                            <br />
                        </div>

                        <div className="PosterEditor__item PosterEditor__item--options">
                            <p className="PosterEditor__item-title">
                                {this.getValue('poster:editor:pick_color')}
                            </p>

                            <div className="PosterEditor__color-picker">
                                <div
                                    className="PosterEditor__color-picker__item PosterEditor__color-picker__item--orange"
                                    onClick={() => this.selectColor('orange')}
                                />
                                <div
                                    className="PosterEditor__color-picker__item PosterEditor__color-picker__item--blue"
                                    onClick={() => this.selectColor('blue')}
                                />
                                <div
                                    className="PosterEditor__color-picker__item PosterEditor__color-picker__item--white border"
                                    onClick={() => this.selectColor('white')}
                                />
                            </div>

                        </div>
                    </div>
                </div>

                <br />
                <br />

                <div className="PosterEditor__preview-title">{this.getValue('poster:editor:poster_preview')}</div>
                <div
                    className="PosterEditor__preview"
                    style={{backgroundImage: `url(${this.state.uploadedImage ? this.state.uploadedImage : this.state.images[this.getChosenImageIndex()].src})`}}
                >
                    <img src="img/poster-template.png" style={{visibility: 'hidden', position: 'relative', zIndex: -1}}/>
                    <Template color={this.state.color} className="PosterEditor__preview__border"/>
                    <div
                        className="PosterEditor__preview__text"
                        style={{color: this.colors[this.state.color]}}
                    >
                        PROTECT<br/>
                        WHAT<br/>
                        YOU<br/>
                        LOVE
                    </div>

                    {/*<div className="PosterEditor__preview__country"></div>*/}

                    <div className="PosterEditor__preview__text--editable">
                        {this.getMessage()}
                        <br />
                        {this.getName()} {this.getCountry()}
                    </div>
                </div>

                <section className="next-step">
                    <button type="submit" className="Button Button--next-step">
                        <img src="img/submit_button.png"/>
                    </button>
                </section>

                <div
                    className={`PosterEditor__image-gallery ${this.state.galleryOpen ? 'PosterEditor__image-gallery--open' : ''}`}>
                    {this.state.images.map((image, index) => (
                        <div
                            key={index}
                            onClick={() => this.select(index)}
                            className={`
                                PosterEditor__image-gallery__img
                                ${image.selected ? 'PosterEditor__image-gallery__img--selected' : ''}
                                ${this.isPrev(index) ? 'PosterEditor__image-gallery__img--prev' : ''}
                                ${this.isNext(index) ? 'PosterEditor__image-gallery__img--next' : ''}
                            `}
                        >
                            <div
                                style={{backgroundImage: `url(${image.src})`}}
                                className="PosterEditor__image-gallery__img-inside"
                            />
                        </div>
                    ))}

                    <div className="PosterEditor__image-gallery__prev" onClick={() => this.prev()}>
                        &lt;
                    </div>
                    <div className="PosterEditor__image-gallery__next" onClick={() => this.next()}>
                        &gt;
                    </div>

                    <div className="PosterEditor__image-gallery__page">
                        {this.getSelectedImageIndex() + 1} / {this.state.images.length}
                    </div>

                    <div className="PosterEditor__image-gallery__close" onClick={() => this.toggleGallery(false)}>
                        &times;
                    </div>
                </div>
            </form>
        );
    }
}

PosterEditor.propTypes = {
    values: PropTypes.objectOf(PropTypes.string).isRequired,
};

const nodes = document.querySelectorAll('.poster-editor-component');

for (let i = 0; i < nodes.length; i++) {
    const node = nodes[i];
    const props = {
        values: JSON.parse(node.getAttribute('data-values') || '{}'),
    };

    render(<PosterEditor {...props}/>, node);
}
