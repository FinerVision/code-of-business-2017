$(document).ready(() => {

    // ImageUpload component
    $('div[data-component=ImageUpload]').each(function () {

        // consts
        const $this = $(this);
        const props = $this.data('props');
        const {name, defaultValue} = props;

        // errors
        if (!name) {
            throw new Error('Attribute data-name is necessary in ImageUpload component. (div[data-component=ImageUpload])');
        }
        if (!window.AssetStore) {
            throw new Error('AssetStore is missing for ImageUpload component, please enclose Asset Store in a <script /> tag for this page')
        }

        // generate all elements inside of the base
        const $input = $(document.createElement('input'))
            .attr({type: 'hidden', name})
            .val(defaultValue);
        const $assetStoreWrap = $(document.createElement('div'))
            .addClass('asset-store-actual');
        const $previewCurrentImage = $(document.createElement('a'))
            .addClass('btn')
            .addClass('btn-success')
            .attr('target', '_blank')
            .attr('href', defaultValue)
            .html('View Current Image');
        const $previewButton = $(document.createElement('a'))
            .addClass('btn')
            .addClass('btn-primary')
            .attr('target', '_blank')
            .html('Preview New Image').hide();
        $this.append($input).append($assetStoreWrap).append($previewCurrentImage).append($previewButton);

        // init asset store
        window.AssetStore.render({
            maxFiles: 1,
            maxFileSize: 10000000,
            mimeTypes: ['image/png', 'image/jpeg', 'image/jpg'],
            onMaxFilesReached: function () {
                // console.log('onMaxFilesReached');
            },
            onMaxFileSizeReached: function () {
                // console.log('onMaxFileSizeReached');
            },
            onInvalidMimeType: function () {
                // console.log('onInvalidMimeType');
            },
            onFileAdded: function (file) {
                // console.log('onFileAdded', file);
            },
            onFileUploaded: function (file, assets) {
                // console.log('onFileUploaded', file, assets);
                const url = assets[0].host + assets[0].path;
                $previewButton.attr('href', url).show();
                $input.val(url);
            },
            onFileRemoved: function (file, assets) {
                // console.log('onFileRemoved', file, assets);
            }
        }, this);
    });

});