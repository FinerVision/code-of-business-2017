<?php

$this->get('/', 'PageController@home');
$this->get('/video', 'PageController@video');
$this->get('/principles', 'PageController@principles');
$this->get('/policies', 'PageController@policies');
$this->get('/pledge', 'PageController@pledge');
$this->get('/poster', 'PageController@poster');
$this->get('/holding', 'PageController@holding');
$this->post('/upload', 'UploadController@upload');
$this->post('/submit', 'PosterController@submit');


// admin routes
$this->get('login', 'LoginController@showLoginForm')->name('login');
$this->post('login', 'LoginController@login');
$this->get('logout', 'LoginController@logout')->name('logout');
$this->post('logout', 'LoginController@logout');
$this->get('/view-posters', function() { return redirect(route('admin.view-posters')); });
$this->get('admin', function() { return redirect(route('admin.view-posters')); });
$this->get('admin/view-posters', 'AdminController@viewPosters')->name('admin.view-posters');
$this->get('admin/edit-page', 'AdminController@pageIndex')->name('admin.edit-page');
$this->post('admin/edit-page', 'AdminController@pageIndexSubmit');
$this->get('admin/edit-page/{pageId}', 'AdminController@pageEdit')->name('admin.edit-page.edit');
$this->post('admin/edit-page/{pageId}', 'AdminController@pageEditSubmit');
