<?php

use Illuminate\Database\Seeder;
use App\User;

class UserSeeder20171128 extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'unilever',
            'email' => 'winghim@finervision.com',
            'password' => bcrypt('pStiG2A5iIj4'),
        ]);
    }
}
