<?php

use App\Content;
use Illuminate\Database\Seeder;

class ContentTableSeederInit extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // TODO : remove this before pushing
        Content::truncate();

        $contents = [

            // home page
            ['Welcome', 'text', 'home:welcome', 'Welcome to Integrity Pledge Week',],
            ['Intro', 'text', 'home:intro', 'Press WATCH to begin your four-step journey to signing the Code Declaration.',],

            // video page
            ['Intro', 'textmulti', 'video:intro', 'At Unilever, we do business with integrity: not just to comply with laws, legislation, and regulations where we operate, but because we are driven by a longstanding commitment to doing the right thing. Since 1995, our Code of Business Principles have set out what we stand for and, together with our 24 Code Policies, how we work. These documents provide a clear and powerful guidance on what is needed from each of us. Since 2016 we have established an annual Business Integrity Pledge: where we all promise, by signing our Code Declaration, to show each other and the world that we believe in the power of our values and have a positive influence on the way business is done.
So, whether you’re a longstanding employee, or new to Unilever, we encourage you to familiarise yourself with the Code of Business Principles and the Code Policies and we ask every employee to sign the annual Code Declaration. By being one of tens of thousands to respond positively to this call to action, you’re affirming your belief that a business that champions Value with Values, will help protect what you love.
'],
            ['Title', 'text', 'video:title', 'it’s how we work',],

            // principles
            ['Intro', 'text', 'principles:intro', 'Our Code of Business Principles is a simple ethical statement of how we operate. We publish this externally and expect all who work with us to meet our high principles.',],
            ['Slogan', 'text', 'principles:slogan', 'Embrace it. Share it. Live it.',],

            // principles - grid
            ['Box 1 - Title', 'text', 'principles:grid:box1:title', 'STANDARD OF CONDUCT',],
            ['Box 1 - Body', 'textmulti', 'principles:grid:box1:body', 'We conduct our operations with honesty, integrity and openness, and with respect for the human rights and interests of our employees.
We shall similarly respect the legitimate interests of those with whom we have relationships.',],
            ['Box 2 - Title', 'text', 'principles:grid:box2:title', 'SHAREHOLDERS',],
            ['Box 2 - Body', 'textmulti', 'principles:grid:box2:body', 'Unilever will conduct its operations in accordance with internationally accepted principles of good corporate governance. We will provide timely, regular and reliable information on our activities, structure, financial situation and performance to all shareholders.',],
            ['Box 3 - Image', 'image', 'principles:grid:box3:image', env('APP_URL') . '/img/flowers.png',],
            ['Box 4 - Image', 'image', 'principles:grid:box4:image', env('APP_URL') . '/img/hands.png',],
            ['Box 5 - Title', 'text', 'principles:grid:box5:title', 'PUBLIC ACTIVITIES'],
            ['Box 5 - Body', 'textmulti', 'principles:grid:box5:body', 'Unilever companies are encouraged to promote and defend their legitimate business interests. Unilever will co-operate with governments and other organisations, both directly and through bodies such as trade associations, in the development of proposed legislation and other regulations which may affect legitimate business interests.
Unilever neither supports political parties nor contributes to the funds of groups whose activities are calculated to promote party interests.'],
            ['Box 6 - Title', 'text', 'principles:grid:box6:title', 'CONSUMERS',],
            ['Box 6 - Body', 'textmulti', 'principles:grid:box6:body', 'Unilever is committed to providing branded products and services which consistently offer value in terms of price and quality, and which are safe for their intended use. Products and services will be accurately and properly labelled, advertised and communicated.',],
            ['Box 7 - Title', 'text', 'principles:grid:box7:title', 'BUSINESS PARTNERS',],
            ['Box 7 - Body', 'textmulti', 'principles:grid:box7:body', 'Unilever is committed to establishing mutually beneficial relations with our suppliers, customers and business partners. In our business dealings we expect our partners to adhere to business principles consistent with our own.',],
            ['Box 8 - Image', 'image', 'principles:grid:box8:image', env('APP_URL') . '/img/elephants.png',],
            ['Box 9 - Title', 'text', 'principles:grid:box9:title', 'EMPLOYEES',],
            ['Box 9 - Body', 'textmulti', 'principles:grid:box9:body', 'Unilever is committed to a working environment that promotes diversity and equal opportunity and where there is mutual trust, respect for human rights and no discrimination.

  We will recruit, employ and promote employees on the sole basis of the qualifications and abilities needed for the work to be performed.

  We are committed to safe and healthy working conditions for all employees.

 We will provide employees with a total remuneration package that meets or exceeds the legal minimum standards or appropriate prevailing industry standards.

We will not use any form of forced, compulsory, trafficked or child labour.

We are committed to working with employees to develop and enhance each individual’s skills and capabilities.

We respect the dignity of the individual and the right of employees to freedom of association and collective bargaining.

We will maintain good communications with employees through company-based information and consultation procedures.

We will ensure transparent, fair and confidential procedures for employees to raise concerns.',],
            ['Box 10 - Title', 'text', 'principles:grid:box10:title', 'COMMUNITY INVOLVEMENT',],
            ['Box 10 - Body', 'textmulti', 'principles:grid:box10:body', 'Unilever strives to be a trusted corporate citizen and, as an integral part of society, to fulfil our responsibilities to the societies and communities in which we operate.',],
            ['Box 11 - Title', 'text', 'principles:grid:box11:title', 'OBEYING THE LAW',],
            ['Box 11 - Body', 'textmulti', 'principles:grid:box11:body', 'Unilever companies and employees are required to comply with the laws and regulations of the countries in which we operate.',],
            ['Box 12 - Image', 'image', 'principles:grid:box12:image', env('APP_URL') . '/img/connect.png',],
            ['Box 13 - Title', 'text', 'principles:grid:box13:title', 'THE ENVIRONMENT',],
            ['Box 13 - Body', 'textmulti', 'principles:grid:box13:body', 'Unilever is committed to making continuous improvements in the management of our environmental impact and to the longer-term goal of developing a sustainable business. Unilever will work in partnership with others to promote environmental care, increase understanding of environmental issues and disseminate good practice.',],
            ['Box 14 - Title', 'text', 'principles:grid:box14:title', 'INNOVATION',],
            ['Box 14 - Body', 'textmulti', 'principles:grid:box14:body', 'In our scientific innovation to meet consumer needs we will respect the concerns of our consumers and of society. We will work on the basis of sound science, applying rigorous standards of product safety.',],
            ['Box 15 - Title', 'text', 'principles:grid:box15:title', 'BRIBERY AND CORRUPTION',],
            ['Box 15 - Body', 'textmulti', 'principles:grid:box15:body', 'Unilever does not give or receive, whether directly or indirectly, bribes or other improper advantages for business or financial gain. No employee may offer, give or receive any gift or payment which is, or may be construed as being, a bribe. Any demand for, or offer of, a bribe must be rejected immediately and reported to management. Unilever accounting records and supporting documents must accurately describe and reflect the nature of the underlying transactions. No undisclosed or unrecorded account, fund or asset will be established or maintained.',],
            ['Box 16 - Image', 'image', 'principles:grid:box16:image', env('APP_URL') . '/img/umbrellas-2.jpg',],
            ['Box 17 - Title', 'text', 'principles:grid:box17:title', 'COMPETITION',],
            ['Box 17 - Body', 'textmulti', 'principles:grid:box17:body', 'Unilever believes in vigorous yet fair competition and supports the development of appropriate competition laws. Unilever companies and employees will conduct their operations in accordance with the principles of fair competition and all applicable regulations.',],
            ['Box 18 - Title', 'text', 'principles:grid:box18:title', 'CONFLICTS OF INTERESTS',],
            ['Box 18 - Body', 'textmulti', 'principles:grid:box18:body', 'All employees and others working for Unilever are expected to avoid personal activities and financial interests which could conflict with their responsibilities to the company. Employees must not seek gain for themselves or others through misuse of their positions.',],
            ['Box 19 - Title', 'text', 'principles:grid:box19:title', 'COMPLIANCE – MONITORING – REPORTING',],
            ['Box 19 - Body', 'textmulti', 'principles:grid:box19:body', 'Compliance with these principles is an essential element in our business success. The Unilever Board is responsible for ensuring these principles are applied throughout Unilever. The Chief Executive Officer is responsible for implementing these principles and is supported in this by the Global Code and Policy Committee which is chaired by the Chief Legal Officer. Day-to-day responsibility is delegated to all senior management of the geographies, categories, functions and operating companies. They are responsible for implementing these principles, supported by local Code Committees. Assurance of compliance is given and monitored each year. Compliance is subject to review by the Board supported by the Corporate Responsibility Committee and for financial and accounting issues the Audit Committee. Any breaches of the Code must be reported. The Board of Unilever will not criticise management for any loss of business resulting from adherence to these principles and other mandatory policies. Provision has been made for employees to be able to report in confidence and no employee will suffer as a consequence of doing so.',],

            // policies
            ['Intro', 'text', 'policies:intro', 'Our 24 Code Policies define the ethical behaviours that we all need to demonstrate when working for Unilever. They are grouped into the five areas outlined above.',],
            ['Download Text', 'text', 'policies:download:text', 'Below is an overview of the relevant policies, for full information view the ',],
            ['Download Link', 'link', 'policies:download:link', 'http://inside.unilever.com/global/codeandcodepolicies/Documents/4394_COBP_Code_Policies_Booklet_INTERNAL%20v13.pdf',],
            ['Download Link Text', 'text', 'policies:download:link_text', 'Code Policies PDF',],

            // policies content
            ['Policies - Governance Title', 'text', 'policies:content:governance:title', 'Governance',],
            ['Policies - Governance Description', 'text', 'policies:content:governance:description', 'Unilever’s reputation for doing business with integrity and with respect for all those with whom it interacts goes back to the very origins of the company. Indeed, the businesses that formed Unilever were among the most enlightened of their time, improving the welfare of their workers and developing products with a positive social impact.',],
            ['Policies - Governance - Title 1', 'text', 'policies:content:governance:item1:title', 'Living the Code',],
            ['Policies - Governance - Description 1', 'text', 'policies:content:governance:item1:description', 'Unilever’s reputation for doing business with integrity and respect for others is an asset, as valuable as its people and its brands. To maintain our reputation requires the highest standards of behaviour.',],
            ['Policies - Governance - Title 2', 'text', 'policies:content:governance:item2:title', 'Legal Consultation',],
            ['Policies - Governance - Description 2', 'text', 'policies:content:governance:item2:description', 'Employees must at all times comply with laws and regulations that apply to the countries in which Unilever operates. Ignorance of the law is no excuse. Timely legal consultation is essential to ensure that Unilever’s legitimate business interests and opportunities are protected.',],
            ['Policies - Governance - Title 3', 'text', 'policies:content:governance:item3:title', 'Responsible Risk Management',],
            ['Policies - Governance - Description 3', 'text', 'policies:content:governance:item3:description', 'Risk management is integral to Unilever’s strategy and to the achievement of Unilever’s long-term goals. Our success as an organisation depends on our ability to identify and exploit the opportunities generated by our business and the markets Unilever operates in.',],

            ['Policies - Countering corruption Title', 'text', 'policies:content:countering_corruption:title', 'Countering corruption',],
            ['Policies - Countering corruption Description', 'text', 'policies:content:countering_corruption:description', 'Integrity defines how we behave, wherever we are. It guides us to do the right thing for the long-term success of Unilever.',],
            ['Policies - Countering corruption - Title 1', 'text', 'policies:content:countering_corruption:item1:title', 'Avoiding Conflicts of Interest',],
            ['Policies - Countering corruption - Description 1', 'text', 'policies:content:countering_corruption:item1:description', 'Conflicts of interest can have a significant negative impact on the reputation and effectiveness of Unilever, its business and its people. This Code Policy sets out what employees must do to avert or manage actual or perceived conflicts of interest.',],
            ['Policies - Countering corruption - Title 2', 'text', 'policies:content:countering_corruption:item2:title', 'Anti-Bribery',],
            ['Policies - Countering corruption - Description 2', 'text', 'policies:content:countering_corruption:item2:description', 'To support global efforts to fight corruption, most countries have laws that prohibit bribery: many apply these ‘internationally’ to behaviour beyond their borders. A breach of such laws may result in fines for Unilever and in personal penalties for individuals. Dealings with public officials are particularly high risk: even the appearance of illegal conduct could cause significant damage to Unilever’s reputation.',],
            ['Policies - Countering corruption - Title 3', 'text', 'policies:content:countering_corruption:item3:title', 'Gifts & Hospitality',],
            ['Policies - Countering corruption - Description 3', 'text', 'policies:content:countering_corruption:item3:description', 'All Unilever’s relationships must reflect its ongoing commitment to doing business with integrity. Hospitality can play a positive role in building relationships with customers, suppliers and other third parties. Likewise, it is sometimes appropriate to offer reasonable gifts, e.g. in the context of promotional events or product launches. However, as accepting or receiving gifts and hospitality can be open to abuse or generate actual or perceived conflicts of interest, this should occur sparingly and always be legitimate and proportionate in the context of Unilever’s business activities.',],
            ['Policies - Countering corruption - Title 4', 'text', 'policies:content:countering_corruption:item4:title', 'Accurate Records, Reporting & Accounting',],
            ['Policies - Countering corruption - Description 4', 'text', 'policies:content:countering_corruption:item4:description', 'The financial reports and other information that Unilever maintains internally and the financial information it provides to shareholders, regulators and other stakeholders must be accurate and complete.',],
            ['Policies - Countering corruption - Title 5', 'text', 'policies:content:countering_corruption:item5:title', 'Protecting Unilever’s Physical & Financial Assets & Intellectual Property',],
            ['Policies - Countering corruption - Description 5', 'text', 'policies:content:countering_corruption:item5:description', 'Employees are responsible for ensuring Unilever’s assets are protected. This Code Policy covers the protection of physical assets/property, financial assets and intellectual property.',],
            ['Policies - Countering corruption - Title 6', 'text', 'policies:content:countering_corruption:item6:title', 'Anti-money Laundering',],
            ['Policies - Countering corruption - Description 6', 'text', 'policies:content:countering_corruption:item6:description', 'To protect Unilever’s reputation and avoid criminal liability, it is important not to become associated – however innocently – with the criminal activities of others. In particular, Unilever and its employees must ensure Unilever does not receive the proceeds of criminal activities, as this can amount to the criminal offence of money laundering. This Code Policy sets out essential steps employees must take to avoid being implicated in money laundering.',],

            ['Policies - Respecting people Title', 'text', 'policies:content:respecting_people:title', 'Respecting people',],
            ['Policies - Respecting people Description', 'text', 'policies:content:respecting_people:description', 'People should be treated with dignity, honesty and fairness. Unilever and its employees celebrate the diversity of people, and respect people for who they are and what they bring. Unilever wants to foster working environments that are fair and safe, where rights are respected and everyone can achieve their full potential.',],
            ['Policies - Respecting people - Title 1', 'text', 'policies:content:respecting_people:item1:title', 'Occupational Health & Safety',],
            ['Policies - Respecting people - Description 1', 'text', 'policies:content:respecting_people:item1:description', 'Unilever is committed to providing healthy and safe working conditions. Unilever complies with all applicable legislation and regulations and aims to continuously improve health and safety performance.',],
            ['Policies - Respecting people - Title 2', 'text', 'policies:content:respecting_people:item2:title', 'Respect, Dignity & Fair Treatment',],
            ['Policies - Respecting people - Description 2', 'text', 'policies:content:respecting_people:item2:description', 'Business can only flourish in societies where human rights are respected, upheld and advanced. Unilever recognises that business has the responsibility to respect human rights and the ability to contribute to positive human rights impacts.',],

            ['Policies - Engaging externally Title', 'text', 'policies:content:engaging_externally:title', 'Engaging externally',],
            ['Policies - Engaging externally Description', 'text', 'policies:content:engaging_externally:description', 'Throughout our value chain, from innovation through to our consumers, Unilever and its employees need to demonstrate the same ethical standards when engaging with others externally as when dealing with colleagues.',],
            ['Policies - Engaging externally - Title 1', 'text', 'policies:content:engaging_externally:item1:title', 'Responsible Innovation',],
            ['Policies - Engaging externally - Description 1', 'text', 'policies:content:engaging_externally:item1:description', 'Innovation is fundamental to Unilever’s business success and a core part of our global strategy. The integrity and objectivity of our Science are a key foundation for our approach to responsible innovation. Safety is non-negotiable.',],
            ['Policies - Engaging externally - Title 2', 'text', 'policies:content:engaging_externally:item2:title', 'Responsible Marketing',],
            ['Policies - Engaging externally - Description 2', 'text', 'policies:content:engaging_externally:item2:description', 'Unilever is committed to developing, producing, marketing and selling all its products and services responsibly. Unilever can and should conduct marketing activities in line with societal expectations.',],
            ['Policies - Engaging externally - Title 3', 'text', 'policies:content:engaging_externally:item3:title', 'Product Quality',],
            ['Policies - Engaging externally - Description 3', 'text', 'policies:content:engaging_externally:item3:description', 'Unilever’s reputation is founded on delighting our consumers and customers with consistently great product quality that meets or exceeds their needs and expectations. Our aim is to be the most trusted and preferred customer and consumer choice on every occasion.',],
            ['Policies - Engaging externally - Title 4', 'text', 'policies:content:engaging_externally:item4:title', 'Responsible Sourcing',],
            ['Policies - Engaging externally - Description 4', 'text', 'policies:content:engaging_externally:item4:description', 'Unilever expects its business partners to adhere to values and principles consistent with our own. Unilever is developing new business practices to grow our company and communities, by doing business in a manner that improves lives of workers across our supply chain, their communities and the environment, consistent with the Unilever Sustainable Living Plan.',],
            ['Policies - Engaging externally - Title 5', 'text', 'policies:content:engaging_externally:item5:title', 'Fair Competition',],
            ['Policies - Engaging externally - Description 5', 'text', 'policies:content:engaging_externally:item5:description', 'Competition laws prohibit anti-competitive agreements (or cartels) between competitors. Many national laws also prohibit abuses of dominant position and include specific rules relating to agreements with distributors and other customers. Investigations by competition authorities may result in significant fines and costs, and damage our reputation. Criminal sanctions may also apply.',],
            ['Policies - Engaging externally - Title 6', 'text', 'policies:content:engaging_externally:item6:title', 'Contact with Government, Regulators & Non-governmental Organisations (NGOs)',],
            ['Policies - Engaging externally - Description 6', 'text', 'policies:content:engaging_externally:item6:description', 'Any contact by employees or other representatives with government, legislators, regulators or NGOs must be done with honesty, integrity, openness and in compliance with local and international laws.',],
            ['Policies - Engaging externally - Title 7', 'text', 'policies:content:engaging_externally:item7:title', 'Political Activities & Political Donations',],
            ['Policies - Engaging externally - Description 7', 'text', 'policies:content:engaging_externally:item7:description', 'Unilever companies are prohibited from supporting or contributing to political parties or candidates. Employees can only offer support and contributions to political groups in a personal capacity. This Code Policy sets out how Unilever employees must manage their business relationship with political groups.',],
            ['Policies - Engaging externally - Title 8', 'text', 'policies:content:engaging_externally:item8:title', 'External Communications – The Media, Investors & Analysts',],
            ['Policies - Engaging externally - Description 8', 'text', 'policies:content:engaging_externally:item8:description', 'Communication with investment communities – including shareholders, brokers and analysts – and the media must be managed carefully. Such communication has important legal requirements and demands specialist skills and experience. Only individuals with specific authorisation and training/briefing may communicate about Unilever with investment communities or the media, or respond to their enquiries or questions.',],

            ['Policies - Safeguarding information Title', 'text', 'policies:content:safeguarding_information:title', 'Safeguarding information',],
            ['Policies - Safeguarding information Description', 'text', 'policies:content:safeguarding_information:description', 'Information is essential to our success: it fuels our research, keeps us in touch with consumer needs and helps us work effectively together. If used inappropriately, information can cause considerable damage to our business.',],
            ['Policies - Safeguarding information - Title 1', 'text', 'policies:content:safeguarding_information:item1:title', 'Protecting Unilever’s Information',],
            ['Policies - Safeguarding information - Description 1', 'text', 'policies:content:safeguarding_information:item1:description', 'Information is one of Unilever’s most valuable business assets: Unilever is committed to safeguarding and protecting our information and any other information entrusted to us.',],
            ['Policies - Safeguarding information - Title 2', 'text', 'policies:content:safeguarding_information:item2:title', 'Preventing Insider Trading',],
            ['Policies - Safeguarding information - Description 2', 'text', 'policies:content:safeguarding_information:item2:description', 'Employees must not use inside information to buy or sell securities of Unilever PLC, Unilever N.V. or any listed Unilever subsidiary, or any other publicly traded company. Securities include shares, equities and related derivatives or spread bets.',],
            ['Policies - Safeguarding information - Title 3', 'text', 'policies:content:safeguarding_information:item3:title', 'Competitors’ Information & Intellectual Property',],
            ['Policies - Safeguarding information - Description 3', 'text', 'policies:content:safeguarding_information:item3:description', 'Unilever respects the intellectual property and confidential information of third parties, including competitors, suppliers and customers. Confidential information is information about another company that is not in the public domain and has value.',],
            ['Policies - Safeguarding information - Title 4', 'text', 'policies:content:safeguarding_information:item4:title', 'Personal Data & Privacy',],
            ['Policies - Safeguarding information - Description 4', 'text', 'policies:content:safeguarding_information:item4:description', 'Unilever respects the privacy of all individuals and the confidentiality of any personal data Unilever holds about them. This Code Policy sets out what steps employees must take to ensure personal data is handled appropriately.',],
            ['Policies - Safeguarding information - Title 5', 'text', 'policies:content:safeguarding_information:item5:title', 'Use of Information Technology',],
            ['Policies - Safeguarding information - Description 5', 'text', 'policies:content:safeguarding_information:item5:description', 'Unilever’s Information Technology (IT) – including desktops and laptops, mobile devices, networks, software, email, data, business applications and internet/intranet – are critical to our operations. This Code Policy explains what employees need to do to ensure the responsible and secure use of IT in Unilever, including compliance with all relevant laws and regulations.',],

            // pledges
            ['Pledge Ticker Title', 'text', 'pledge:ticker:title', 'Global Pledges',],
            ['Pledge Ticker Number', 'text', 'pledge:ticker:number', '032787',],
            ['Pledge Ticker Statement', 'text', 'pledge:ticker:statement', 'Watch out for regular updates',],
            ['Pledge Signing Statement', 'text', 'pledge:signing:statement', 'By signing your annual Code Declaration you are contributing to our Integrity Pledge for Unilever. Click below to make your pledge.',],
            ['Pledge Button Caption', 'text', 'pledge:button:caption', 'Please disable pop up blockers before you click.',],
            ['Pledge Button Link', 'text', 'pledge:button:link', 'https://unilever.csod.com/samldefault.aspx?ouid=2&returnurl=%252fDeepLink%252fProcessRedirect.aspx%253fmodule%253dlodetails%2526lo%253d6e05a1a9-87e7-44fc-bf65-bfb8154b0089',],
            ['Pledge Button Text', 'text', 'pledge:button:text', 'Pledge',],
            ['Pledge Gallery Intro', 'text', 'pledge:gallery:intro', 'Once you\'ve pledged create your own integrity poster. Here are the latest posters created by your colleagues.',],

            // poster
            ['Poster Intro Title', 'text', 'poster:intro:title', "MAKE\nYOUR OWN\nINTEGRITY\nPOSTER"],
            ['Poster Intro Body', 'text', 'poster:intro:body', "What better way to share your commitment to integrity by voicing what you stand for and creating your own poster!\nUsing our poster generator below you can upload your own image, or chose one from our gallery, adding a short statement telling us what you love and protect, or what integrity looks like for you.\nBe sure to download your poster after and share it on Chatter using #BIpledge.\nYou can also look through other posters created by your colleagues in the gallery below."],

            // poster - editor
            ['Poster Editor - Message Title', 'text', 'poster:editor:message_title', 'Tell us what you love, what you protect, or what integrity looks like for you'],
            ['Poster Editor - Name Title', 'text', 'poster:editor:name_title', 'Enter Your Name'],
            ['Poster Editor - Country / MCO / Unit Title', 'text', 'poster:editor:country_title', 'Enter Your country/MCO or unit'],
            ['Poster Editor - Upload Title', 'text', 'poster:editor:upload_title', 'Drop or click here to upload your own image! [5MB max]'],
            ['Poster Editor - Uploaded Text', 'text', 'poster:editor:uploaded_text', 'Uploaded!'],
            ['Poster Editor - Choose From Gallery', 'text', 'poster:editor:choose_from_gallery_text', 'Or, choose from our gallery of images'],
            ['Poster Editor - Pick Color', 'text', 'poster:editor:pick_color', 'Pick a Color'],
            ['Poster Editor - Preview', 'text', 'poster:editor:poster_preview', 'Your Poster Preview'],
            ['Poster Editor - Submitting', 'text', 'poster:editor:submitting', 'Submitting...'],

        ];

        foreach($contents as $content) {
            Content::create([
                'title' => $content[0],
                'type' => $content[1],
                'key' => $content[2],
                'value' => $content[3],
            ]);
        }
    }
}
