<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Content extends Model
{

    private static $cachedValues = [];
    private static $cachedPrefixes = [];

    public static function cacheValueWithPrefix(string $keyPrefix) {

        if (in_array($keyPrefix, self::$cachedPrefixes)) {
            return;
        }

        $cachedPrefixes[] = $keyPrefix;

        $queryResult = static::where('key', 'LIKE', "{$keyPrefix}%")->get();

        // assign values into cache
        if (!$queryResult->isEmpty()) {
            $queryResult->each(function($content) {
                self::$cachedValues[$content->key] = $content->value;
            });
        }

    }

    public static function byKey(string $key) {

        // check if the value is cached
        if (isset(self::$cachedValues[$key])) {
            return self::$cachedValues[$key];
        }

        // fetch the first part of the key
        $keyParts = explode(':', $key);
        $keyPrefix = current($keyParts);

        // cache all the value with the such key prefix
        self::cacheValueWithPrefix($keyPrefix);

        return self::$cachedValues[$key] ?? null;
    }

    /**
     *
     * fetch the value by key without caching
     *
     * @param string $key
     */
    public static function byKeySingle(string $key) {
        return static::where(['key' => $key])->first()->value;
    }


    /**
     *
     * return the Content object by input the keyPrefix
     * !!! NOTE : nothing is cached when you are calling this function
     *
     * @param string $keyPrefix
     * @return mixed
     */
    public static function byKeyPrefix(string $keyPrefix) {
        return Content::where('key', 'LIKE', "{$keyPrefix}%")->orderBy('id', 'ASC')->get();
    }

    public static function pairsByKeyPrefix(string $keyPrefix) {

        $output = [];

        foreach(self::$cachedValues as $key => $value) {
            // check if the key is started with the such prefix
            if (strpos($key, $keyPrefix) === 0) {
                $output[$key] = $value;
            }
        }
        
        return $output;

    }

    public static function latestEditingByPrefix(string $keyPrefix) {
        $queryResult = static::where('key', 'LIKE', "{$keyPrefix}%")->orderBy('updated_at', 'DESC')->first();
        return !is_null($queryResult) ? $queryResult->updated_at : null;
    }

}
