<?php

use App\Content;

function getActive($path)
{
    if (is_array($path)) {
        $paths = implode('|', $path);
        return request()->is("{$paths}*") ? 'active' : '';
    }

    return isActive($path) ? 'active' : '';
}

function isActive($path)
{
    return request()->is("{$path}*") ? 'active' : '';
}


// sugar
function currentRoute() {
    return \Request::route()->getName();
}


function isCurrentRoute($routeName) {
    return currentRoute() === $routeName;
}


function classNames(...$params) {
    $outputArray = [];

    for($i = 0; $i < count($params); $i++) {

        $item = $params[$i];

        switch(gettype($item)) {
            case 'string':
                $outputArray[] = $item;
                break;
            case 'array':
                foreach($item as $key => $value) {
                    if (is_numeric($key)) {
                        $outputArray[] = $value;
                    } else if ($value) {
                        $outputArray[] = $key;
                    }
                }
                break;
        }

    }

    return implode(' ', $outputArray);
}

function content(string $key) {
    return Content::byKey($key);
}

function contentLines(string $key) {

}

function contentPTags(string $key) {
    $value = Content::byKey($key);
    $value = strip_tags(trim($value));
    $lines = array_filter(explode("\n", $value));
    $outputLines = array_map(function($line) { return "<p>{$line}</p>"; }, $lines);
    return implode('', $outputLines);
}

function contentGroup(string $keyPrefix) {
    return Content::pairsByKeyPrefix($keyPrefix);
}
