<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'password' => 'required'
        ]);

        $name = $request->get('name');
        $password = $request->get('password');

        $authenticated = Auth::attempt(compact('name', 'password'));

        if (!$authenticated) {
            return redirect()->to('login');
        }

        return redirect()->to('view-posters');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->to('login');
    }
}
