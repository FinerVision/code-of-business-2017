<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Constraint;
use Intervention\Image\ImageManager;

class UploadController extends Controller
{
    public function upload(Request $request)
    {
        $file = $request->file('file');
        $hash = time() . '-' . bin2hex(openssl_random_pseudo_bytes(16));

        $manager = new ImageManager();
        $image = $manager->make($file->getRealPath())->fit(1000, 1415, function (Constraint $constraint) {
            $constraint->aspectRatio();
        });
        $fileName = "{$hash}.jpg";
        $image->orientate();
        $image->save(public_path("uploads/{$fileName}"));

        // TODO: Handle S3 upload errors
        $uploaded = Storage::disk('s3')->put("uploads/{$fileName}", (string)$image->encode());

        $region = env('AWS_REGION');
        $bucket = env('AWS_BUCKET');

        return [
            'success' => $uploaded,
            'url' => "https://s3.{$region}.amazonaws.com/{$bucket}/uploads/{$fileName}"
        ];
    }
}
