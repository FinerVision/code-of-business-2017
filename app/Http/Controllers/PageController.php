<?php

namespace App\Http\Controllers;

use App\Poster;
use Carbon\Carbon;

class PageController extends Controller
{
    public function __construct()
    {
        $now = Carbon::now('Europe/London');
        $release = Carbon::createFromDate(2017, 11, 16, 'Europe/London');
        $release->setTime(0, 0, 0);

        if (env('APP_DEBUG') || env('APP_OVERRIDE')) {
            return;
        }

        if ($now->gt($release) && request()->path() === 'holding') {
            redirect()->to('/')->send();
        }

        if ($now->lt($release) && request()->path() !== 'holding') {
            redirect()->to('/holding')->send();
        }
    }

    public function home()
    {
        return view('pages.home');
    }

    public function video()
    {
        return view('pages.video');
    }

    public function principles()
    {
        $principles = collect(config()->get('unilever.principles'));
        return view('pages.principles', compact('principles'));
    }

    public function policies()
    {
        $policies = collect(config()->get('unilever.policies'));
        return view('pages.policies', compact('policies'));
    }

    public function pledge()
    {
        $posters = Poster::latest()->take(30)->get();
        $total = str_pad(trim(content('pledge:ticker:number')), 6, '0', STR_PAD_LEFT);


        return view('pages.pledge', compact('posters', 'total'));
    }

    public function poster()
    {
        $posters = Poster::latest()->take(30)->get();
        return view('pages.poster', compact('posters'));
    }

    public function holding()
    {
        return view('pages.holding');
    }
}
