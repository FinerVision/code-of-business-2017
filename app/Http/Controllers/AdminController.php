<?php

namespace App\Http\Controllers;

use App\Session;
use Illuminate\Http\Request;
use Illuminate\Support\MessageBag;

class AdminController extends Controller
{

    public static function verify() {

        $cookie = request()->cookie(config('session.cookie'));

        if (is_null($cookie)) {
            return redirect()->to('login')->send();
        }

        $session = Session::where(['id' => $cookie])->first();

        if (is_null($session) || !$session->getAttribute('user_id')) {
            return redirect()->to('login')->send();
        }

    }

    public function viewPosters() {

        // verify
        self::verify();

        \Poster::where('country', 'NOT LIKE', '%russia%')->orderBy('id', 'DESC')->paginate(16);

        return view('auth.pages.view-posters', [
            'postersPaginate' => \Poster::where('country', 'NOT LIKE', '%russia%')->orderBy('id', 'DESC')->paginate(16),
        ]);
    }


    public function pageIndex() {

        // verify
        self::verify();

        return view('auth.pages.edit-page.index');
    }

    /**
     *
     * when /admin/edit-page is getting post request
     *
     * @param Request $request
     *
     */
    public function pageIndexSubmit(Request $request) {

        // handle submit
        $params = $request->all();

        // message bag for error handling
        $messageBag = new MessageBag();

        // check the pledge ticker number if it's submitted
        if ($params['tickerNumber']) {
            if (!$params['tickerNumber']) {
                $messageBag->add('pledge_ticker', "Please enter a valid number for the ticker's number, recieved input: \"{$params['tickerNumber']}\"");
                return redirect()->back()->withErrors($messageBag);
            }
            \Content::where(['key' => 'pledge:ticker:number'])->update(['value' => $params['tickerNumber']]);
        }

        // redirect using get method
        return redirect(route('admin.edit-page'));

    }


    public function pageEdit($pageKey) {

        // verify
        self::verify();

        // check if the pageKey param is not matching any site key in the config set
        $pageName = config("admin.sitePages.{$pageKey}", null);
        if (is_null($pageName)) {
            return redirect(route('admin.edit-page'));
        }

        // fetch all the fields from the database
        $contents = \Content::byKeyPrefix($pageKey);

        // if the result is empty, still redirecting back to index page
        if ($contents->isEmpty()) {
            return redirect(route('admin.edit-page'));
        }

        // edit page's view
        return view('auth.pages.edit-page.edit', compact('pageKey', 'pageName', 'contents'));

    }

    /**
     *
     * @param $pageKey
     */
    public function pageEditSubmit(string $pageKey, Request $request) {
        // verify
        self::verify();

        // check if the pageKey param is not matching any site key in the config set
        $pageName = config("admin.sitePages.{$pageKey}", null);
        if (is_null($pageName)) {
            return redirect(route('admin.edit-page'));
        }

        // get all contents by page key
        $contents = \Content::byKeyPrefix($pageKey);
        $messageBag = new MessageBag();
        $newValues = $request->all();

        foreach($contents as $content) {
            $hasError = false;
            $newValue = $request->get($content->name);

            switch($content->type) {
                case 'image':
                case 'link':
                    // validate if it's a valid URL
                    if (!filter_var($newValue, FILTER_VALIDATE_URL)) {
                        $messageBag->all($content->name, "Error in input: {$content->title}");
                    }
                    break;
                case 'textmulti':
                case 'text':
                default:
                // cannot be null
                if (is_null($newValue)) {
                    $messageBag->all($content->name, "Please insert a value for: {$content->title}");
                }
            }
        }

        // redirect back if there is any errors
        if ($messageBag->any()) {
            return redirect(route('admin.edit-page.edit', $pageKey))->withInput($request->all())->withErrors($messageBag);
        }

        // if there isn't any errors, assign new value
        foreach($contents as $content) {
            // don't do it if it's the same, reduce memory
            $newValue = $newValues[$content->key];
            if ($content->value !== $newValue) {
                $content->value = $newValue;
                $content->save();
            }
        }

        // done, redirect to the next page
        return redirect(route('admin.edit-page'));

    }

}
