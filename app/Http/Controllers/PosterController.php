<?php

namespace App\Http\Controllers;

use App\Poster;
use Illuminate\Support\Facades\Storage;

class PosterController extends Controller
{
    public function submit()
    {
        // TODO: Check if image file exists and throw an error if not.

        $this->validate(request(), [
            'name' => 'required|max:100',
            'message' => 'required|max:44',
            'country' => 'required|max:44',
            'color' => 'required|in:white,orange,blue',
            'image' => 'required',
        ], [
            'name.required' => 'Please enter your name',
            'message.required' => 'Please enter your message',
            'country.required' => 'Please enter your country/MCO or unit',
            'color.required' => 'Please select a color',
            'image.required' => 'Please select an image',
        ]);

        $image = request()->get('image');
        $name = request()->get('name');
        $message = request()->get('message');
        $country = request()->get('country');
        $color = request()->get('color');
        $template = public_path("img/preview/{$color}.png");

        $region = env('AWS_REGION');
        $bucket = env('AWS_BUCKET');
        $url = "https://s3.{$region}.amazonaws.com/{$bucket}";
        $imageName = str_replace("{$url}/uploads/", '', $image);
        $image = storage_path("app/{$imageName}");

        // TODO: Think of a more efficient way of doing this
        if (!\File::exists($image)) {
            $imageData = file_get_contents("{$url}/uploads/{$imageName}");
            file_put_contents($image, $imageData);
        }

        $html = view('components.preview', compact('color', 'image', 'template', 'name', 'message', 'country'))->render();
        $fileName = time() . '-' . bin2hex(openssl_random_pseudo_bytes(16)) . '.jpg';
        $poster = "posters/{$fileName}";

        \SnappyImage::loadHTML($html)->save(public_path($poster));

        // TODO: Handle S3 upload errors
        Storage::disk('s3')->put($poster, file_get_contents(public_path($poster)));

        // Reset poster and image variables to their remote URLs.
        $poster = "{$url}/posters/{$fileName}";
        $image = "{$url}/uploads/{$imageName}";

        return Poster::create(compact('image', 'name', 'message', 'country', 'color', 'poster'));
    }

}
