# Unilever Code of Business 2017

### Development

```
$ npm run dev
```

Will watch scss and js files in resources/assets for changes and compile them to the public directory.

### Deployment

##### Staging

```
$ npm run deploy-staging
```

Will deploy to [https://apps.finervision.com/CodeOfBusiness2017/public/](https://apps.finervision.com/CodeOfBusiness2017/public/)

##### Production

```
$ npm run deploy-production
```

Will deploy to [http://integritypledge.unilever.com/](http://integritypledge.unilever.com/)

### Notes

Live release is 20th November 2017. This is setup to automatically redirect
from /holding to /.
