#!/usr/bin/env bash

ssh -i ~/.ssh/unilever_web1 integritypledge_admin@52.57.120.7 -T <<EOF
	cd /data/www.integritypledge.unilever.com/site
	echo "Pulling from master..."
	echo -e ""
	git stash
	git pull
	git stash clear
	exit
EOF

ssh -i ~/.ssh/unilever_web2 integritypledge_admin@52.57.99.172 -T <<EOF
	cd /data/www.integritypledge.unilever.com/site
	echo "Pulling from master..."
	echo -e ""
	git stash
	git pull
	git stash clear
	exit
EOF
