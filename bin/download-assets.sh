#!/usr/bin/env bash

if [ -z $(command -v wget) ]; then
    echo "The command 'wget' does not exist. Install this command to continue";
fi

wget https://s3.eu-west-2.amazonaws.com/fv-unilever/CodeOfBusiness/hero.mp4 -O public/vid/hero.mp4
