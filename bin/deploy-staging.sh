#!/usr/bin/env bash

ssh -i ~/.ssh/samantha.pem ubuntu@ec2-52-38-225-26.us-west-2.compute.amazonaws.com -T <<EOF
	cd /var/www/apps/public/CodeOfBusiness2017
	echo "Pulling from master..."
	echo -e ""
	git stash
	git pull
	git stash clear
	echo "Installing dependencies..."
	echo -e ""
	composer install
	composer dump-autoload
	npm install
	echo "Running build..."
	echo -e ""
	npm run build
	exit
EOF
